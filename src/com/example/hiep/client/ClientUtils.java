package com.example.hiep.client;

import com.google.gwt.core.client.JavaScriptObject;

public class ClientUtils {

	public static native void log(String msg) /*-{
		$wnd.console.log(msg);
	}-*/;
	
	public static native void log(JavaScriptObject object) /*-{
		$wnd.console.log('Object', object);
	}-*/;
	
	public static native void showLoadingAjax() /*-{
		$wnd.console.log("showLoadingAjax");
	}-*/;
	
	public static native void hideLoadingAjax() /*-{
		$wnd.console.log("hideLoadingAjax");
	}-*/;
}
