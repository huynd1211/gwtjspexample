package com.example.hiep.client.activities.widget;

import com.example.hiep.client.ClientUtils;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;

public class VimeoPlayer extends FlowPanel {

	private final String IFRAME_ID_PANEL = "video_id_panel_main";
	
	public VimeoPlayer() {
		initVideo(this.getElement(), IFRAME_ID_PANEL);
		final String videoId = "180837766";
		String url = "https://player.vimeo.com/video/" + videoId + "?quality=540p";
		loadedByUrl(url, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				ClientUtils.log("loadedByUrl videoId " + videoId);
			}
			
			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}
	
	private native void initVideo(Element parentElement, String iframeIdPanel) /*-{
		$wnd.initVideo(parentElement, iframeIdPanel);
	}-*/;
	
	private native void loadedByUrl(String url, AsyncCallback<Void> callback) /*-{
		$wnd.loadedUrl(url, function(){
			
			//callback.@com.google.gwt.user.client.rpc.AsyncCallback::onSuccess(Ljava/lang/Object;)(null);
		});
	}-*/;
}
