package com.example.hiep.client.activities.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.hiep.client.ClientUtils;
import com.example.hiep.client.activities.ClientFactory;
import com.example.hiep.client.activities.basic.BasicWebActivity;
import com.example.hiep.client.activities.home.HomePlace;
import com.example.hiep.shared.VideoInfo;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

public class TestActivity extends BasicWebActivity {

	private TestView view;
	public TestActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getTestView();
		super.start(panel, eventBus, view);
		panel.setWidget(view);
	}
	
	@Override
	protected void bind() {
		super.bind();
		addHandlerRegistration(view.getButtonPrev().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				ClientUtils.log("Click button prev");
				goTo(new HomePlace());
			}
		}));
	}
	
	@Override
	protected void loadData() {
		super.loadData();
		loadVideos();
	}

	private void loadVideos() {
		List<VideoInfo> videoInfos = new ArrayList<VideoInfo>();
		List<Long> videoIds = new ArrayList<Long>(Arrays.asList(254972480l, 254972521l, 254972575l, 254972708l, 254972767l, 254972792l));
		for(int i = 0; i < videoIds.size(); i++) {
			VideoInfo videoInfo = new VideoInfo();
			videoInfo.setId(videoIds.get(i));
			videoInfo.setTitle("Video name mã số " + videoIds.get(i));
			videoInfos.add(videoInfo);
		}
		view.showVideos(videoInfos);
	}
}
