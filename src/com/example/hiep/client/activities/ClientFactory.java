package com.example.hiep.client.activities;

import com.example.hiep.client.activities.cropper.CropperView;
import com.example.hiep.client.activities.home.HomeView;
import com.example.hiep.client.activities.test.TestView;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

public interface ClientFactory {
	PlaceController getPlaceController();
	EventBus getEventBus();
	HomeView getHomeView();
	TestView getTestView();
	CropperView getCropperView();
}
