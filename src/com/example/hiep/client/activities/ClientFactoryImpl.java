package com.example.hiep.client.activities;

import com.example.hiep.client.activities.cropper.CropperView;
import com.example.hiep.client.activities.cropper.CropperViewImpl;
import com.example.hiep.client.activities.home.HomeView;
import com.example.hiep.client.activities.home.HomeViewImpl;
import com.example.hiep.client.activities.test.TestView;
import com.example.hiep.client.activities.test.TestViewImpl;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

public class ClientFactoryImpl implements ClientFactory {
	private SimpleEventBus eventBus;
	private PlaceController placeController;
	private HomeView homeView;
	private TestView testView;
	private CropperView cropperView;
	
	public ClientFactoryImpl() {
		eventBus = new SimpleEventBus();
		placeController = new PlaceController(eventBus);
	}
	
	@Override
	public PlaceController getPlaceController() {
		return placeController;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public HomeView getHomeView() {
		if(homeView == null) {
			homeView = new HomeViewImpl();
		}
		return homeView;
	}

	@Override
	public TestView getTestView() {
		if(testView == null) {
			testView = new TestViewImpl();
		}
		return testView;
	}

	@Override
	public CropperView getCropperView() {
		if(cropperView == null) {
			cropperView = new CropperViewImpl();
		}
		return cropperView;
	}
}
