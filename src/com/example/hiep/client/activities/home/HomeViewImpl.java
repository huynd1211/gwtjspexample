package com.example.hiep.client.activities.home;

import com.example.hiep.client.activities.basic.BasicWebViewImpl;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class HomeViewImpl extends BasicWebViewImpl implements HomeView {
	private static HomeViewImplUiBinder uiBinder = GWT.create(HomeViewImplUiBinder.class);
	
	interface HomeViewImplUiBinder extends UiBinder<Widget, HomeViewImpl> {
	}
	
	@UiField HTMLPanel mainPanel;
	private Button buttonNext = new Button("NextPlace");
	
	public HomeViewImpl() {	 
		super();
		this.layout.getContainerPanel().add(uiBinder.createAndBindUi(this));
		mainPanel.add(buttonNext);
	}
	
	@Override
	public HasClickHandlers getButtonNext() {
		return buttonNext;
	}
}
