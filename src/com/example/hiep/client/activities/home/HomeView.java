package com.example.hiep.client.activities.home;

import com.example.hiep.client.activities.basic.BasicWebView;
import com.google.gwt.event.dom.client.HasClickHandlers;

public interface HomeView extends BasicWebView{
	HasClickHandlers getButtonNext();

}
