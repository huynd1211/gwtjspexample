package com.example.hiep.shared;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class UserInfo implements IBasic{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id private String id;
	@Index private Long createDate;
	@Index private Long lastUpdate;
	private String password = "";
	private String name = "";
	private String avatar = "";
	
	@Ignore private int loginCode = 0;
	@Ignore private UserHobby hobby = null;
	@Ignore protected String session = null;
	
	public UserInfo(){
	}

	public UserInfo(String id, String password, String name){ 
		setId(id);
		setPassword(password);
		setName(name);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Long getCreateDate() {
		return createDate;
	}
	public Long getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public void setCreateDate(Long createDate) {
		this.createDate = createDate;
	}
	public UserHobby getHobby() {
		return hobby;
	}
	public void setHobby(UserHobby hobby) {
		this.hobby = hobby;
	}
	public int getLoginCode() {
		return loginCode;
	}
	public void setLoginCode(int loginCode) {
		this.loginCode = loginCode;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	@Override
	public int getStatus() {
		return 0;
	}
}
