package com.example.hiep.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class UserHobby implements IBasic {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id private Long id;
	@Index private String userId;
	@Index private Long createDate;
	@Index private Long lastUpdate;
	private Map<String, String> mapSongStatus = new HashMap<String, String>();
	
	public UserHobby(){
	}

	public UserHobby(Long id){ 
		setId(id);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCreateDate() {
		return createDate;
	}
	public Long getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public void setCreateDate(Long createDate) {
		this.createDate = createDate;
	}
	
	public List<String> getSongsLove() {
		List<String> songIds = new ArrayList<String>();
		for(Map.Entry<String, String> entry : mapSongStatus.entrySet()) {
			if(Integer.parseInt(entry.getValue()) != Config.STATUS_LOVE_LEVEL_DELETE) {
				songIds.add(entry.getKey());
			}
		}
		return songIds;
	}
	
	public void addSongLoveLevel(String songId, String level) {
		mapSongStatus.put(songId, level);
	}

	@Override
	public int getStatus() {
		return 0;
	}
}
